---
title: Introduction
linkTitle: Documentation
---

👋 Hello! Welcome to the Vai documentation!

## What is Vai?

A simple task runner. Imagine GitHub actions and Makefile had a baby.

## Features

- Simple syntax and usage
- Remote and local imports (wip)
- Matrix support (design in progress)
- Task input and output style similar to GitHub actions

## Questions or Feedback?

{{< callout emoji="❓" >}}
  Vai is still in active development.
  Have a question or feedback? Feel free to [open an issue](https://github.com/noxsios/vai/issues)!
{{< /callout >}}
