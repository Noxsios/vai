---
title: ''
toc: false
---

Vai: a simple task runner.

## Installation

```sh
go install github.com/noxsios/vai/cmd/vai@latest
```

![demo](https://github.com/Noxsios/vai/assets/50058333/850b79e5-4ebf-4b59-8e29-95102f50d759)

## Next

{{< cards >}}
  {{< card link="docs/schema" title="Schema" icon="document-duplicate" >}}
  {{< card link="docs/examples#run-a-task-with-variables" title="Variables" icon="template" >}}
  {{< card link="docs/examples#run-a-task-from-a-remote-file" title="Remote Tasks" icon="server" >}}
  {{< card link="docs/examples#passing-outputs-between-steps" title="Inputs and Outputs" icon="chart-square-bar" >}}
{{< /cards >}}
