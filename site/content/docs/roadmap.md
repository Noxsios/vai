---
title: Roadmap
draft: true
---

TODO:

- handle local paths for imports
- add matrix support
- add more examples
- get coverage to ~80%
- get E2E tests covering all the golden paths
- import / publish to OCI?
- run a docker container as a task
- SBOMing?
- CONTRIBUTING.md
- CODE_OF_CONDUCT.md
- SECURITY.md
- ISSUE_TEMPLATE.md
- PULL_REQUEST_TEMPLATE.md
- CHANGELOG.md
- Add SPDX?
- outputs from a "uses" task
- directory traversal outside of repo using relative path in a remote task
- add `if` and `continue-on-error` fields?
- `--dry-run` flag
