---
title: Design
draft: true
---

- syntax
- import system
- inspiration from Makefiles and GitHub Actions
- CLI UX
- notes about complexity due to indentation
- limitations of Go's type system when it comes to external structured data
- unit tests vs e2e tests and the frameworks used
- https://www.mail-archive.com/help-make@gnu.org/msg02778.html
- cwd vs cd
